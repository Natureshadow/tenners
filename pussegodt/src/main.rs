use btleplug::api::{Central, Manager as _, ScanFilter};
use btleplug::platform::Manager;

use std::error::Error;

use std::time::Duration;
use tokio::time;

use ratatui::{
    prelude::*,
    widgets::{Block, Borders, Gauge},
};
use tui_big_text::BigTextBuilder;

use pussegodt::models::api::*;
use pussegodt::models::*;

#[tokio::main]
async fn main() -> std::result::Result<(), Box<dyn Error>> {
    // startup: Enable raw mode for the terminal, giving us fine control over user input
    crossterm::terminal::enable_raw_mode()?;
    crossterm::execute!(std::io::stderr(), crossterm::terminal::EnterAlternateScreen)?;

    // Initialize the terminal backend using crossterm
    let mut terminal = Terminal::new(CrosstermBackend::new(std::io::stderr()))?;

    let manager = Manager::new().await.unwrap();

    // get the first bluetooth adapter
    let adapters = manager.adapters().await?;
    let central = adapters.into_iter().next().unwrap();

    // start scanning for devices
    central.start_scan(ScanFilter::default()).await?;
    time::sleep(Duration::from_secs(2)).await;
    central.stop_scan();

    let brush = OralBIo6::find_first_device(&central)
        .await
        .unwrap()
        .unwrap();

    loop {
        let brushtime = brush.brushtime().await;
        let pressure = brush.pressure().await;

        terminal.draw(|frame| {
            let main_layout = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(vec![Constraint::Percentage(50), Constraint::Percentage(50)])
                .split(frame.size());

            let brushtime_layout = Layout::default()
                .direction(Direction::Vertical)
                .constraints(vec![Constraint::Percentage(75), Constraint::Percentage(25)])
                .split(main_layout[0]);

            let pressure_layout = Layout::default()
                .direction(Direction::Vertical)
                .constraints(vec![Constraint::Percentage(75), Constraint::Percentage(25)])
                .split(main_layout[1]);

            match brushtime {
                Ok(Some(duration)) => {
                    let seconds = duration.as_secs() % 60;
                    let minutes = (duration.as_secs() / 60) % 60;

                    let gauge = Gauge::default()
                        .block(Block::default().borders(Borders::ALL).title("Brushtime"))
                        .gauge_style(
                            Style::default()
                                .fg(if duration.as_secs() < 120 {
                                    Color::LightRed
                                } else if duration.as_secs() < 180 {
                                    Color::LightYellow
                                } else if duration.as_secs() < 240 {
                                    Color::LightGreen
                                } else {
                                    Color::LightRed
                                })
                                .bg(Color::Black)
                                .add_modifier(Modifier::ITALIC),
                        )
                        .percent((duration.as_secs() * 100 / 180).try_into().unwrap());
                    frame.render_widget(gauge, brushtime_layout[0]);

                    let big_text = BigTextBuilder::default()
                        .style(Style::new().blue())
                        .lines(vec![format!("{:0>2}:{:0>2}", minutes, seconds)
                            .white()
                            .into()])
                        .build()
                        .unwrap();
                    frame.render_widget(big_text, brushtime_layout[1]);
                }
                Ok(None) | Err(_) => {}
            };
            match pressure {
                Ok(Some(pressure)) => {
                    let gauge = Gauge::default()
                        .block(Block::default().borders(Borders::ALL).title("Pressure"))
                        .gauge_style(
                            Style::default()
                                .fg(match pressure {
                                    Pressure::Correct(_) => Color::Green,
                                    Pressure::TooLight(_) => Color::LightBlue,
                                    Pressure::TooHard(_) => Color::LightRed,
                                    Pressure::Unknown(_) => Color::White,
                                })
                                .bg(Color::Black)
                                .add_modifier(Modifier::ITALIC),
                        )
                        .percent(match pressure {
                            Pressure::Correct(_) => 50,
                            Pressure::TooLight(_) => 20,
                            Pressure::TooHard(_) => 100,
                            Pressure::Unknown(_) => 0,
                        });
                    frame.render_widget(gauge, pressure_layout[0]);

                    let big_text = BigTextBuilder::default()
                        .style(Style::new().blue())
                        .lines(vec![format!("{}", pressure).white().into()])
                        .build()
                        .unwrap();
                    frame.render_widget(big_text, pressure_layout[1]);
                }
                Ok(None) | Err(_) => {}
            };
        })?;

        // Check for user input every 250 milliseconds
        if crossterm::event::poll(std::time::Duration::from_millis(250))? {
            // If a key event occurs, handle it
            if let crossterm::event::Event::Key(key) = crossterm::event::read()? {
                if key.kind == crossterm::event::KeyEventKind::Press {
                    match key.code {
                        crossterm::event::KeyCode::Char('q') => break,
                        _ => {}
                    }
                }
            }
        }
    }

    // shutdown down: reset terminal back to original state
    crossterm::execute!(std::io::stderr(), crossterm::terminal::LeaveAlternateScreen)?;
    crossterm::terminal::disable_raw_mode()?;

    Ok(())
}
