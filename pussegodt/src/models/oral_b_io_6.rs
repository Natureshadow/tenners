use super::api::*;

use btleplug::api::Peripheral as _;
use btleplug::platform::Peripheral;
use btleplug::Result;

use uuid::{uuid, Uuid};

use std::time::Duration;

use packed_struct::prelude::*;

// Types reverse-engineered from the Oral-B Android app
//  from sources/codes/alchemy/oralb/blesdk/data/characteristic/model

// ======= FF04 =======
#[derive(PrimitiveEnum_u8, Clone, Copy, Debug, PartialEq)]
pub enum State {
    Unknown = 0,
    Init = 1,
    Idle = 2,
    Run = 3,
    Charge = 4,
    Setup = 5,
    FlightMenu = 6,
    ChargeForbidden = 7,
    PreRun = 8,
    PostRun = 9,
    FinalTest = 113,
    PCBTest = 114,
    Sleep = 115,
    Transport = 116,
    CalibrationTest = 117,
}

#[derive(PrimitiveEnum_i8, Clone, Copy, Debug, PartialEq)]
pub enum SubState {
    Unknown = -1,
    TransportDisabledDeactivateTimerDisabled = 0,
    TransportEnabledDeactivateTimerDisabled = 1,
    TransportEnabledDeactivateTimerEnabled = 3,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "2")]
pub struct DeviceState {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub state: State,
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub sub_state: SubState,
}
// ====================

// ======= FF05 =======
// FIXME find diff between Oral-B app and reverse-engineered type
#[derive(Debug, PartialEq, PackedStruct)]
#[packed_struct(size_bytes = 4)]
pub struct BatteryLevel {
    pub level: u8,
    #[packed_field(size_bytes = "3", endian = "msb")]
    _reserved: u32,
}
// ====================

// ======= FF06 =======
// FIXME find diff between Oral-B app and reverse-engineered type
#[derive(Debug, PartialEq, PackedStruct)]
#[packed_struct(size_bytes = 4)]
pub struct ButtonState {
    #[packed_field(size_bytes = "1")]
    pub power_pressed: bool,
    #[packed_field(size_bytes = "1")]
    pub mode_pressed: bool,
    #[packed_field(size_bytes = "2", endian = "msb")]
    _reserved: u16,
}
// ====================

// ======= FF07 =======
#[derive(PrimitiveEnum_i8, Clone, Copy, Debug, PartialEq)]
pub enum Mode {
    Unknown = -1,
    Clean = 0,
    Soft = 1,
    Massage = 2,
    Polish = 3,
    Turbo = 4,
    SoftPlus = 5,
    Tongue = 6,
    Off = 7,
    Settings = 8,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "1")]
pub struct BrushingMode {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub mode: Mode,
}
// ====================

// ======= FF08 =======
#[derive(Debug, PartialEq, PackedStruct)]
#[packed_struct(size_bytes = 2)]
pub struct BrushingTime {
    pub minutes: u8,
    pub seconds: u8,
}
// ====================

// ======= FF09 =======
// FIXME seems always to be unknown for this brush?
#[derive(PrimitiveEnum_i8, Clone, Copy, Debug, PartialEq)]
pub enum Quadrant {
    Unknown = -2,
    FirstQuadrant = 0,
    SecondQuadrant = 1,
    ThirdQuadrant = 2,
    FourthQuadrant = 3,
    FifthQuadrant = 4,
    SixthQuadrant = 5,
    SeventhQuadrant = 6,
    EigthQuadrant = 7,
    LastQuadrant = -1,
    NoQuadrantDefined = -16,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "3")]
pub struct ToothbrushQuadrant {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub quadrant: Quadrant,
    pub number_of_quadrants: u8,
    _reserved: u8,
}
// ====================

// ======= FF0A =======
#[derive(PrimitiveEnum_u8, Clone, Copy, Debug, PartialEq)]
pub enum Face {
    Off = 0,
    Standard = 1,
    Special2 = 2,
    Special3 = 3,
    Special4 = 4,
    Special5 = 5,
    Special6 = 6,
    Special7 = 7,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "1")]
pub struct Smiley {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub face: Face,
}
// ====================

// ======= FF0B =======
#[derive(PrimitiveEnum_u8, Clone, Copy, Debug, PartialEq)]
pub enum PressureState {
    LowPressure = 0,
    NormalPressure = 1,
    HighPressure = 2,
}

// FIXME Oral-B app reads two booleans first, why?
#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "10")]
pub struct Pressure {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub state: PressureState,
    #[packed_field(endian = "lsb")]
    pub record_a_timestamp: u16,
    #[packed_field(endian = "lsb")]
    pub record_a_pressure: u16,
    #[packed_field(endian = "lsb")]
    pub record_b_timestamp: u16,
    #[packed_field(endian = "lsb")]
    pub record_b_pressure: u16,
    pub identifier: u8,
}
// ====================

// ======= FF0D =======
#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "6")]
pub struct CallibrationData {
    #[packed_field(endian = "lsb")]
    pub x: u16,
    #[packed_field(endian = "lsb")]
    pub y: u16,
    #[packed_field(endian = "lsb")]
    pub z: u16,
}

#[derive(PrimitiveEnum_i8, Clone, Copy, Debug, PartialEq)]
pub enum DashboardStatus {
    SessionIDInvalid = -16,
    FirstPackage = 1,
    PackagesPending = 2,
    LastPackage = 8,
}

// FIXME no idea what this is used for, does not seem to be part of regular data
#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "15")]
pub struct DashboardData {
    #[packed_field(size_bytes = "1", ty = "enum")]
    pub status: DashboardStatus,
    #[packed_field(endian = "lsb")]
    pub timestamp: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_x: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_y: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_z: u16,
    #[packed_field(endian = "lsb")]
    pub motion_x: u16,
    #[packed_field(endian = "lsb")]
    pub motion_y: u16,
    #[packed_field(endian = "lsb")]
    pub motion_z: u16,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "14")]
pub struct GyroMotionData {
    #[packed_field(endian = "lsb")]
    pub timestamp: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_x: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_y: u16,
    #[packed_field(endian = "lsb")]
    pub gyro_z: u16,
    #[packed_field(endian = "lsb")]
    pub motion_x: u16,
    #[packed_field(endian = "lsb")]
    pub motion_y: u16,
    #[packed_field(endian = "lsb")]
    pub motion_z: u16,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "6")]
pub struct HighResolutionMotionData {
    #[packed_field(endian = "lsb")]
    pub motion_x: u16,
    #[packed_field(endian = "lsb")]
    pub motion_y: u16,
    #[packed_field(endian = "lsb")]
    pub motion_z: u16,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "8")]
pub struct MotionData {
    #[packed_field(endian = "lsb")]
    pub timestamp: u16,
    #[packed_field(endian = "lsb")]
    pub motion_x: u16,
    #[packed_field(endian = "lsb")]
    pub motion_y: u16,
    #[packed_field(endian = "lsb")]
    pub motion_z: u16,
}

#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "3")]
pub struct SensorVector {
    #[packed_field(endian = "lsb")]
    pub x: i8,
    #[packed_field(endian = "lsb")]
    pub y: i8,
    #[packed_field(endian = "lsb")]
    pub z: i8,
}

// FIXME only a test
#[derive(PackedStruct, Debug, PartialEq)]
#[packed_struct(size_bytes = "20")]
pub struct SensorData {
    #[packed_field(endian = "lsb")]
    pub timestamp1: u16,
    #[packed_field(size_bytes = "3")]
    pub gyro1: SensorVector,
    #[packed_field(size_bytes = "3")]
    pub motion1: SensorVector,
    #[packed_field(endian = "lsb")]
    pub timestamp2: u16,
    #[packed_field(size_bytes = "3")]
    pub gyro2: SensorVector,
    #[packed_field(size_bytes = "3")]
    pub motion2: SensorVector,
    #[packed_field(endian = "lsb")]
    _reserved: u32,
}
// ====================

pub struct OralBIo6(Peripheral);

impl BrushLowLevelApi for OralBIo6 {
    type PressureRawType = u8;

    const PERIPHERAL_NAME: &'static str = "Oral-B Toothbrush";

    const BRUSH_DURATION_UUID: Option<Uuid> = Some(uuid!("a0f0ff08-5047-4d53-8208-4f72616c2d42"));
    const PRESSURE_UUID: Option<Uuid> = Some(uuid!("a0f0ff0b-5047-4d53-8208-4f72616c2d42"));

    fn peripheral(&self) -> &Peripheral {
        &self.0
    }

    async fn from_peripheral(peripheral: Peripheral) -> Result<Self> {
        peripheral.connect().await?;
        peripheral.discover_services().await?;
        Ok(Self(peripheral))
    }
}
