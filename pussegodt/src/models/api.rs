use btleplug::api::{Central, Characteristic, Peripheral as _};
use btleplug::platform::{Adapter, Peripheral};
use btleplug::Result;

use uuid::Uuid;

use std::time::Duration;

use derive_more::Display;

#[derive(Debug, Display)]
pub enum Pressure<RawValue: Into<f64> + std::fmt::Display> {
    #[display(fmt = "Unknown ({_0})")]
    Unknown(RawValue),
    #[display(fmt = "Too light ({_0})")]
    TooLight(RawValue),
    #[display(fmt = "Correct ({_0})")]
    Correct(RawValue),
    #[display(fmt = "Too hard ({_0})")]
    TooHard(RawValue),
}

pub trait BrushLowLevelApi
where
    Self: Sized,
{
    const PERIPHERAL_NAME: &'static str;
    const PRESSURE_UUID: Option<Uuid>;
    const BRUSH_DURATION_UUID: Option<Uuid>;

    type PressureRawType: Into<f64> + std::fmt::Display;

    fn peripheral(&self) -> &Peripheral;
    async fn from_peripheral(peripheral: Peripheral) -> Result<Self>;

    async fn find_first_device(central: &Adapter) -> Option<Result<Self>> {
        for p in central.peripherals().await.unwrap() {
            if p.properties()
                .await
                .unwrap()
                .unwrap()
                .local_name
                .iter()
                .any(|name| name.contains(Self::PERIPHERAL_NAME))
            {
                return Some(Self::from_peripheral(p).await);
            }
        }
        None
    }

    fn brushtime_characteristic(&self) -> Option<Characteristic> {
        match Self::BRUSH_DURATION_UUID {
            None => None,
            Some(uuid) => self
                .peripheral()
                .characteristics()
                .iter()
                .find(|c| c.uuid == uuid)
                .cloned(),
        }
    }

    fn pressure_characteristic(&self) -> Option<Characteristic> {
        match Self::PRESSURE_UUID {
            None => None,
            Some(uuid) => self
                .peripheral()
                .characteristics()
                .iter()
                .find(|c| c.uuid == uuid)
                .cloned(),
        }
    }
}

pub trait BrushHighLevelApi: BrushLowLevelApi {
    async fn brushtime(&self) -> Result<Option<Duration>> {
        Ok(None)
    }

    async fn pressure(&self) -> Result<Option<Pressure<Self::PressureRawType>>> {
        Ok(None)
    }
}
